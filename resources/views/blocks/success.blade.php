@if(session('success'))
    <div class="alert alert-success">
        {{ $message }}
    </div>
@endif
