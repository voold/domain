@extends('layouts.app')

@section('title', 'Обновление DNS')

@section('content')
<div class="row">

<div class="mx-auto mt-3 col-lg-6">
    @include('blocks.nav')
    <h4 class="mb-3">Обновление ДНС</h4>

    @include('blocks.success', ['message' => 'DNS отредактирован'])
    @include('blocks.errors')

    <form method="post" action="{{ route('domain.dns.update') }}">
        <div class="row g-3">
            <div class="col-12">
                <label for="name" class="form-label">Домен</label>
                <input type="text" class="form-control" name="name" value="{{ old('domain') }}" id="name" required>
            </div>
            <div class="col-12">
                <label for="ns" class="form-label">Список NS</label>
                <textarea class="form-select" name="nservers" id="ns" required>{{ old('ns') }}</textarea>
            </div>

            <button class="my-5 w-100 btn btn-primary btn-lg" type="submit">Отправить</button>
            @csrf
        </form>
</div>
</div>
@endsection
