@extends('layouts.app')

@section('title', 'Создание домена')

@section('content')
<div class="row">

<div class="mx-auto mt-3 col-lg-6">
    @include('blocks.nav')
    <h4 class="mb-3">Регистрация домена</h4>

    @include('blocks.success', ['message' => 'Клиент и домен созданы.'])
    @include('blocks.errors')

    <form method="post" action="{{ route('domain.store') }}">
        <div class="row g-3">
            <div class="col-12">
                <label for="domain" class="form-label">Домен</label>
                <input type="text" class="form-control" name="domain" value="{{ old('domain') }}" id="domain" placeholder="example.ru" required>
            </div>
            <div class="col-12">
                <label for="legal" class="form-label">Юридическая организация</label>
                <select class="form-select" name="legal" id="legal" required>
                    <option value="org">Юридическое лицо</option>
                    <option value="person" selected>Физическое лицо</option>
                    <option value="proprietor">Индивидуальный предприниматель</option>
                </select>
            </div>
            <div class="col-12">
                <label for="nameLocal" class="form-label">ФИО</label>
                <input type="text" class="form-control" name="nameLocal" value="{{ old('nameLocal') }}" id="nameLocal" required>
            </div>

            <div class="col-12">
                <label for="birthday" class="form-label">День рождения</label>
                <input type="date" class="form-control" name="birthday" value="{{ old('birthday') }}" id="birthday" required>
            </div>

            <div class="row gy-3">
                <div class="col-md-6">
                    <label for="email" class="form-label">Email</label>
                    <input type="text" class="form-control" name="emails[]" value="{{ old('emails.0') }}" id="email" required>
                </div>

                <div class="col-md-6">
                    <label for="phone" class="form-label">Телефон</label>
                    <input type="text" class="form-control" name="phones[]" value="{{ old('phones.0') }}" id="phone" placeholder="+7 999 001 00 00" required>
                </div>
            </div>

            <hr class="my-4">
            <h5 class="">Адрес</h5>

            <div class="row gy-3">
                <div class="col-md-2">
                    <label for="index" class="form-label">Индекс</label>
                    <input type="text" class="form-control" name="addressLocal[index]" value="{{ old('addressLocal.index') }}" id="index" required>
                </div>
                <div class="col-md-3">
                    <label for="country" class="form-label">Страна</label>
                    <select class="form-select" name="addressLocal[country]" value="{{ old('addressLocal.coountry') }}" id="country" required>
                        <option value="RU">Россия</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="city" class="form-label">Город</label>
                    <input type="text" class="form-control" name="addressLocal[city]" value="{{ old('addressLocal.city') }}" id="city" required>
                </div>
                <div class="col-md-4">
                    <label for="street" class="form-label">Улица</label>
                    <input type="text" class="form-control" name="addressLocal[street]" value="{{ old('addressLocal.street') }}" id="street" required>
                </div>
            </div>

            <hr class="my-4">
            <h5 class="">Паспорт</h5>

            <input type="hidden" name="identity[type]" value="passport">
            <div class="row gy-3">
                <div class="col-md-6">
                    <label for="series" class="form-label">Серия</label>
                    <input type="text" class="form-control" name="identity[series]" value="{{ old('identity.series') }}" id="series" required>
                </div>

                <div class="col-md-6">
                    <label for="number" class="form-label">Номер</label>
                    <input type="text" class="form-control" name="identity[number]" value="{{ old('identity.number') }}" id="number" required>
                </div>
            </div>

            <div class="col-12">
                <label for="issuer" class="form-label">Кем выдан</label>
                <input type="text" class="form-control" name="identity[issuer]" value="{{ old('identity.issuer') }}" id="issuer" required>
            </div>

            <div class="col-12">
                <label for="issued" class="form-label">Дата выдачи</label>
                <input type="date" class="form-control" name="identity[issued]" value="{{ old('identity.issued') }}" id="issued" required>
            </div>

            <button class="my-5 w-100 btn btn-primary btn-lg" type="submit">Отправить</button>
            @csrf
        </form>
</div>
</div>
@endsection
