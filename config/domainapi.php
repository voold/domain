<?php

return [
    'tci' => [
        'url' => 'https://vrdemo.virtreg.ru/vr-api',
        'login' => 'demo',
        'password' => 'demo',
        'data-format' => 'Y-m-d',
    ],
];
