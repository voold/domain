<?php

namespace App\Repositories;

use App\APIs\TCI\TCIApi;
use App\Models\{Client, Domain};

class ApiRepository implements Interfaces\ApiRepositoryInterface
{
    protected $api;

    public function __construct()
    {
        $this->api = new TCIApi();
    }

    /**
     * Создает клиента. Возвращает его ID
     * @param App\Models\Client $client
     * @return int — ID клиента
     */
    public function createClient(Client $client): int
    {
        $request = $this->api->request('clientCreate', [
            'client' => $client->get()->except('id')
        ]);

        $client->id = $request['id'];

        return $client->id;
    }

    /**
     * Отправляет запрос на создание домена, возвращает его ID
     * @param App\Models\Domain $domain
     * @param App\Models\Client $client
     * @return int — ID домена
     */
    public function createDomain(Domain $domain, Client $client): int
    {
        $request = $this->api->request('domainCreate', [
            'domain' => [
                'name' => $domain->name,
            ]
        ], $client->id);

        $domain->id = $request['id'];

        return $domain->id;
    }

    /**
     * Отправляет запрос на изменения NS записей домена
     * @param App\Models\Domain $domain
     * @param array $params – параметры которые нужно изменить
     */
    public function updateDomain(Domain $domain, array $params): bool
    {
        // подгружаем данные
        if(is_null($domain->id)) {
            $this->getDomainInfo($domain);
        }

        $request = $this->api->request('domainUpdate', [
            'id' => $domain->id,
            'domain' => $domain->get()->only($params)
        ], $domain->clientId);

        return true;
    }

    /**
     * Получает информацию о домене
     * @return array
     */
    public function getDomainInfo(Domain $domain): bool
    {
        $request = $this->api->request('domainEnum', [
            'query' => [
                'filter' => [
                    'name', '~', $domain->name
                ]
            ]
        ]);

        $domain->set($request['domains'][0], true);

        return true;
    }
}
