<?php

namespace App\Repositories\Interfaces;

use App\Models\{Domain, Client};

interface ApiRepositoryInterface
{
    public function createDomain(Domain $domain, Client $client): int;
    public function updateDomain(Domain $domain, array $params): bool;

    public function createClient(Client $client): int;
}
