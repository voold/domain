<?php

namespace App\Models;

class Domain extends Model
{
    public $id;
    public $name;
    public $nservers;
    public $delegated;

    /**
     * Конвертирует NS из строки в массив
     * @param string $nservers – NS в виде строки
     * @return array – NS в виде массива
     */
    public function convertNS(string $nservers): array
    {
        $nservers = explode("\r\n", $this->nservers);
        return $nservers;
    }

    /**
     * Rонвертирует домен в ACE или IDN в зависимости от параметра $to
     * @param string $domain - домен в формате "name.zone"
     * @param string $to - по умолчанию 'ACE'
     * @return string
     */
    public function convert(string $domain, string $to = 'ace'): string
    {
        $to = strtolower($to);

        switch($to) {
            case 'ace': $domain = idn_to_ascii($domain, IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46); break;
            case 'idn': $domain = idn_to_utf8($domain, IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46); break;
            default: throw new \Exception("Can't convert domain to {$to}");
        }

        return $domain;
    }
}
