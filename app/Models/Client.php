<?php

namespace App\Models;

class Client extends Model
{
    public $id;
    public $legal;
    public $nameLocal;
    public $birthday;
    public $emails = [];
    public $phones = [];
    public $addressLocal = [
        'index', 'country', 'city', 'street'
    ];
    public $identity = [
        'type', 'series', 'number', 'issuer', 'issued'
    ];
}
