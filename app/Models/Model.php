<?php

namespace App\Models;

class Model
{
    public function __construct(array $params)
    {
        $this->set($params);
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function get()
    {
        return collect(get_object_vars($this));
    }

    /**
     * Устанавливает значения модели из массива
     * @param bool $skip_if_exists - по умолчанию false. В зависимости от параметра, при установке значения оно не будет
     *                               перезаписываться, если уже существует
     */
    public function set(array $params, bool $skip_if_exists = false)
    {
        foreach($params as $key => $value) {
            if($skip_if_exists && isset($this->$key)) {
                continue;
            }

            $this->$key = $value;
        }
    }
}
