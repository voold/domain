<?php

namespace App\APIs\TCI;

use Illuminate\Support\Facades\Http;
use Datto\JsonRpc\Responses\ErrorResponse;
use App\APIs\Api;

class TCIApi extends Api
{
    public function __construct()
    {
        $this->company = 'tci';
        $this->token = null;
    }

    /**
     * Авторизовывает в апи, получает токен для дальнешйих обращений
     * @return bool
     */
    protected function auth(): string
    {
        if(isset($this->token)) {
            return $this->token;
        }

        $request = $this->request('authLogin', [
            'login' => $this->getFromConfig('login'),
            'password' => $this->getFromConfig('password')
        ], null, false);

        $this->token = $request['token'];

        return $this->token;
    }

    /**
     * Делает запрос к апи, возвращает ответ в виде массива
     * @param string $method - метод апи
     * @param array $params - необязательный, параметры которые будут переданы апи
     * @param int $clientId - необязательный, айди клиента, если он требуется в запросе
     * @param bool $withToken - настройки передачи токена в запросе (в случае если ранее он был получен) по умолчанию true
     * @return array
     */
    public function request(string $method, array $params = [], int $clientId = null, bool $withToken = true): array
    {
        // проверяем нужно ли добавлять в запрос токен
        if($withToken) {
            $params['auth']['token'] = $this->auth();
        }

        // если в запросе нужен clientId - прописываем
        if(!is_null($clientId)) {
            $params['clientId'] = $clientId;
        }

        // подготавливаем запрос
        $request = new \Datto\JsonRpc\Http\Client($this->getFromConfig('url'));
        $query = $request->query($method, $params, $response);

        // отправляем
        $request->send();

        // проверяем ошибки
        if($response instanceof ErrorResponse) {
            throw new \Exception('Api returned an error:'. $response->getCode() .', '. $response->getMessage());
        }

        return $response;
    }
}
