<?php

namespace App\APIs;

abstract class Api
{
    // используется для поиска значений в конфиге
    protected $company;
    // токен для авторизации при запросах к апи
    protected $token;

    abstract protected function auth(): string;
    abstract public function request(string $method, array $params, int $clientId, bool $withToken): array;

    /**
     * Функция возвращающая объект апи для работы с клиентами
     * @return ClientApi
     */
    public function client(): ClientApi
    {
        return $this->client;
    }

    /**
     * Функция возвращающая объект апи для работы с доменами
     * @return DomainApi
     */
    public function domain(): DomainApi
    {
        return $this->domain;
    }

    /**
     * Получает значение из конфига
     * @param string $param - имя значения
     * @return mixed
     */
    protected function getFromConfig(string $param)
    {
        $param = "domainapi.{$this->company}.{$param}";
        if(\Config::has($param)) {
            return \Config::get($param);
        }
        else {
            throw new \Exception("Can't find {$param} in config");
        }
    }
}
