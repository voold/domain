<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DomainRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'domain' => 'required',
            'legal' => 'required|in:org,person,proprietor',
            'nameLocal' => 'required',
            'birthday' => 'required|date_format:Y-m-d',

            'emails.*' => 'required|email',
            'phones.*' => 'required|regex:#\+7\s\d{3}\s\d{3}\s\d{2}\s\d{2}#',

            'addressLocal.index' => 'required|max:16',
            'addressLocal.country' => 'required',
            'addressLocal.city' => 'required',
            'addressLocal.street' => 'required',

            'identity.type' => 'required|in:passport',
            'identity.series' => 'required|max:10',
            'identity.number' => 'required|max:15',
            'identity.issuer' => 'required|max:128',
            'identity.issued' => 'required|date_format:Y-m-d',
        ];
    }
}
