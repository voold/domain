<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DomainRequest;
use App\Http\Requests\DomainUpdateDNSRequest;
use App\Repositories\ApiRepository;
use App\Models\{Client, Domain};

class DomainController extends Controller
{
    protected $api;

    public function __construct(ApiRepository $api)
    {
        $this->api = $api;
    }


    public function create()
    {
        return view('domain.create');
    }

    public function store(DomainRequest $request)
    {
        // получаем данные клиента из формы
        $client = $request->except(['domain', '_token']);
        // получаем имя создаваемого домена
        $domain = $request->safe()->only('domain')['domain'];

        $client = new Client($client);
        $domain = new Domain(['name' => $domain]);

        // создаем клиента и домен
        $this->api->createClient($client);
        $this->api->createDomain($domain, $client);

        return back()->with('success', true);
    }

    public function editDNS()
    {
        return view('domain.update-dns');
    }

    public function updateDNS(DomainUpdateDNSRequest $request)
    {
        $request = $request->validated();

        $domain = new Domain($request);
        // пока не понял как конвертацию имени домена и строки с ns в массив
        // перенести в момент установки или получения значения из Domain (т.е. внутрь класса Domain)
        // "не понял как" — в смысле без создания костыля. Так что пока тут:
        $domain->set([
            'name' => $domain->convert($domain->name),
            'nservers' => $domain->convertNS($domain->nservers)
        ]);

        // обновляем NS
        $this->api->updateDomain($domain, ['nservers', 'delegated']);

        return back()->with('success', true);
    }
}
