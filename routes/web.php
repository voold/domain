<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DomainController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'domain', 'as' => 'domain.'], function() {
    Route::get('create', [DomainController::class, 'create'])->name('create');
    Route::post('store', [DomainController::class, 'store'])->name('store');

    Route::get('update/dns', [DomainController::class, 'editDNS'])->name('dns.edit');
    Route::post('update/dns', [DomainController::class, 'updateDNS'])->name('dns.update');
});

Route::get('/', function () { return redirect(route('domain.create')); });
